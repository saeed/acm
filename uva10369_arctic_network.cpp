#include <cmath>
#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include <cstring>
#include <cstdio>
using namespace std;

#define MAXN 501

bool sat[MAXN];

struct UF
{
	int id[MAXN];
	int sz[MAXN];
	void init(int start,int end)
	{
		for ( int i=start;i<=end;++i )
		{
			id[i]=i;
			sz[i]=1;
		}
	}
	int root (int p)
	{
		while ( id[p]!=p )
		{
			id[p]=id[id[p]];
			p = id[p];
		}
		return p;
	}
	void merge ( const int & p,const int &q )
	{
		int pId = root(p);
		int qId = root(q);
		if ( sz[pId] > sz[qId] )
		{
			sz[pId] += sz[qId];
			id[qId] = id[pId];
		}
		else
		{
			sz[qId] += sz[pId];
			id[pId] = id[qId];
		}
	}
};

struct Edge
{
	int x,y;double w;
	Edge(int _x=0,int _y=0,double _w=0):x(_x),y(_y),w(_w) {}
	bool operator < ( const Edge &e) const {return w < e.w;}

};

double dist2 ( pair<int,int> a , pair<int,int> b )
{
	return ( a.first-b.first )*( a.first-b.first )
		+( a.second-b.second )*( a.second-b.second );
}

int main()
{
	//freopen("in.in","r",stdin);
	//freopen("out.out","w",stdout);
	int tests ; cin>>tests;
	//memset(sat,0,sizeof(sat));
	for ( int i=0;i<tests;++i )
	{
		memset(sat,0,sizeof(sat));
		int s,n;
		cin >> s >> n;
		UF uf;
		uf.init(0,n-1);
		vector<Edge> edges;
		vector<pair<int,int> > nodes;
		for ( int i=0;i<n;++i )
		{
			int a,b;cin>>a>>b;
			nodes.push_back( make_pair(a,b) );
		}
		for ( int i=0;i<n-1;++i )
		{
			for ( int j=i+1;j<n;++j )
			{
				edges.push_back(Edge(i,j,dist2(nodes[i],nodes[j])));
			}
		}
		sort(edges.begin(),edges.end());

		vector<Edge>mst;

		for ( int i=0;i<edges.size();++i )
		{
			if ( uf.root(edges[i].x) != uf.root(edges[i].y) )
			{
				mst.push_back(edges[i]);
				uf.merge(edges[i].x,edges[i].y);
			}
		}

		int nta = s-1;

		reverse(mst.begin(),mst.end());

		if ( nta >= mst.size() )
			printf("0.00\n");
		else
			printf("%.2f\n",sqrt(mst[nta].w));
		

	}
	
}


