//#include <set>
#include <vector>
//#include <map>
#include <iostream>
#include <cstdio>
using namespace std;

#define MAXN 101
#define INF 1000*1000*1000

int main()
{
	//freopen("in.in","r",stdin);

	int a,b,test=0;
	//set<int> nodesSet;
	vector< pair<int,int> > edges;
	//map<int,int> m;
	int fw [MAXN][MAXN];
	while (cin>>a>>b)
	{
		if ( a==b && b==0 && edges.size()>0 )
		{
			//int j=0;
			//for ( set<int>::iterator it = nodesSet.begin(); it!=nodesSet.end(); ++it,++j )
			//{
			//	m[*it]=j;
			//}
			int n = MAXN;
			for ( int I=0;I<n;++I )
				for ( int J=0;J<n;++J )
					fw[I][J]= (I==J?0:INF);
			for ( int i = 0 ; i < edges.size() ; ++i )
			{
				pair<int,int> e = edges[i];
				fw[e.first][e.second] = 1;
			}
			for ( int k=0;k<n;++k )
				for ( int I=0;I<n;++I )
					for ( int J=0;J<n;++J )
						fw[I][J]=std::min(fw[I][J],fw[I][k]+fw[k][J]);

			int sum=0;	
			int count =0 ;
			for ( int I=0;I<n;++I )
				for ( int J=0;J<n;++J )
					if ( fw[I][J]!=0 && fw[I][J]!=INF )
					{
						sum+=fw[I][J];
						++count;
					}
		++test;
		printf("Case %d: average length between pages = %.3f clicks\n",test,1.0*sum/count);

//		nodesSet.clear();
		edges.clear();
//		m.clear();
		}
		else
		{
			//nodesSet.insert(a);
			//nodesSet.insert(b);
			edges.push_back(make_pair(a,b));
		}
	}
	

}