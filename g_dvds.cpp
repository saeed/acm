#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <fstream>
using namespace std;

#define SIZE 20000000

long long mx[SIZE];
long long mn[SIZE];
long long arr[SIZE];

void make ( long long node , long long start , long long end )
{	
	if ( start<end )
	{
		long long mid = (start+end)/2;
		make ( node*2 , start,mid );
		make ( node*2+1,mid+1,end);
		mx[node]=std::max(mx[node*2],mx[node*2+1]);
		mn[node]=std::min(mn[node*2],mn[node*2+1]);
	}
	else
	{
		mx[node]=mn[node]=start;
	}
}

bool query ( long long node, long long start, long long end , long long qr, long long ql )
{
	if ( (start >= qr && end <= ql) )
	{
		if ( qr<=mn[node] && ql>=mx[node] )
			return true;
		return false;
	}
	if ( start < end )
	{
		if ( (qr>=start && qr<=end)||(ql>=start && ql<=end) )
		{
			long long mid = (start+end)/2;
			bool q1= query(node*2,start,mid,qr,ql);
			bool q2 = query(node*2+1,mid+1,end,qr,ql);
			return (q1 && q2);
		}
	}
	return true;
}

void update ( long long node,long long start,long long end,long long pos,long long value )
{
	if ( pos >=start && pos <= end)
	{
		if ( start < end )
		{
			long long mid = (start+end)/2;
			update(node*2,start,mid,pos,value);		
			update(node*2+1,mid+1,end,pos,value);
			mn[node]=std::min(mn[node*2],mn[node*2+1]);
			mx[node]=std::max(mx[node*2],mx[node*2+1]);
		}
		else
		{
			mn[node]=mx[node]=value;
		}
	}
}

int main()
{
	freopen("in.in","r",stdin);
	freopen("out.out","w",stdout);
	long long ntest;cin>>ntest;
	while ( ntest-- )
	{
		long long n,k;
		cin>>n>>k;
		for ( long long i=0;i<n;++i )
			arr[i]=i;
		make(1,0,n-1);
		for ( long long i=0;i<k;++i )
		{
			long long a,b,c;
			cin>>a>>b>>c;
			if ( a==0 )
			{
				long long v1 = arr[b];
				long long v2 = arr[c];
				update(1,0,n-1,b,v2);
				update(1,0,n-1,c,v1);
				arr[c]=v1;
				arr[b]=v2;
			}
			else
			{
				if ( query(1,0,n-1,b,c) )
					cout << "YES" << endl;
				else
					cout << "NO" << endl;
			}
		}
	}

}