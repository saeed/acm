#include <cmath>
#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
using namespace std;

#define MAXN 10000

struct UF
{
	int id[MAXN];
	int sz[MAXN];
	void init(int start,int end)
	{
		for ( int i=start;i<=end;++i )
		{
			id[i]=i;
			sz[i]=1;
		}
	}
	int root (int p)
	{
		while ( id[p]!=p )
		{
			id[p]=id[id[p]];
			p = id[p];
		}
		return p;
	}
	void merge ( const int & p,const int &q )
	{
		int pId = root(p);
		int qId = root(q);
		if ( sz[pId] > sz[qId] )
		{
			sz[pId] += sz[qId];
			id[qId] = id[pId];
		}
		else
		{
			sz[qId] += sz[pId];
			id[pId] = id[qId];
		}
	}
};

struct Edge
{
	int x,y;int w;
	Edge(int _x=0,int _y=0,int _w=0):x(_x),y(_y),w(_w) {}
	bool operator < ( const Edge &e) const {return w < e.w;}

};


int main()
{
	//freopen("in.in","r",stdin);
	//freopen("out.out","w",stdout);
	while ( true )
	{
		int n,m;
		cin >> n >> m;
		if ( n==m && m==0 )
			break;
		UF uf;
		uf.init(0,n-1);
		vector<Edge> edges;
		//int total =0;
		for ( int i=0;i<m;++i )
		{
			int a,b,c;cin>>a>>b>>c;
		//	total+=c;
			edges.push_back(Edge(a,b,c));
		}
		sort(edges.begin(),edges.end());

		//int ans=0;
		vector<int> vres;
		
		for ( int i=0;i<edges.size();++i )
		{
			if ( uf.root(edges[i].x) != uf.root(edges[i].y) )
			{
			//	ans+=edges[i].w;
				uf.merge(edges[i].x,edges[i].y);
			}
			else
			{
				vres.push_back(edges[i].w);
			}
		}

		for ( int i=0;i<vres.size();++i )
		{
			cout << vres[i] ;
			if ( i!=vres.size()-1 ) cout << " ";
		}
		if ( vres.size()==0 )
			cout << "forest";
		cout << endl;
		
	}
	
}


