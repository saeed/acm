#include <cmath>
#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
using namespace std;

#define MAXN 10000

struct UF
{
	int id[MAXN];
	int sz[MAXN];
	void init(int start,int end)
	{
		for ( int i=start;i<=end;++i )
		{
			id[i]=i;
			sz[i]=1;
		}
	}
	int root (int p)
	{
		while ( id[p]!=p )
		{
			id[p]=id[id[p]];
			p = id[p];
		}
		return p;
	}
	void merge ( const int & p,const int &q )
	{
		int pId = root(p);
		int qId = root(q);
		if ( sz[pId] > sz[qId] )
		{
			sz[pId] += sz[qId];
			id[qId] = id[pId];
		}
		else
		{
			sz[qId] += sz[pId];
			id[pId] = id[qId];
		}
	}
};

struct Edge
{
	int x,y;double w;
	Edge(int _x=0,int _y=0,double _w=0):x(_x),y(_y),w(_w) {}
	bool operator < ( const Edge &e) const {return w < e.w;}

};


double dist2 ( pair<int,int> a , pair<int,int> b )
{
	return ( a.first-b.first ) * ( a.first-b.first ) + ( a.second-b.second ) * ( a.second-b.second ) ;
}

int main()
{
	//freopen("in.in","r",stdin);
	//freopen("out.out","w",stdout);
	int testCases;
	cin >> testCases;
	for ( int test=1;test<=testCases;++test)
	{
		int n,r;
		cin>>n>>r;
		double r2 = r*r;
		UF uf1;
		uf1.init(0,n);
		vector< pair<int,int> > nodes;
		vector<Edge> edges;

		for ( int i=0;i<n;++i )
		{
			int a,b;cin>>a>>b;
			nodes.push_back(make_pair(a,b));
		}

		for ( int i=0;i<n-1;++i )
		{
			for ( int j=i+1;j<n;++j )
			{
				double d = dist2(nodes[i],nodes[j]);
				if ( uf1.root(i) != uf1.root(j) )
				{
					if ( d < r2 )
					{
						uf1.merge(i,j);
					}
				}
				edges.push_back( Edge(i,j,d) );
			}
		}

		set<int> statesSet;
		for ( int i=0;i<n;++i )
			statesSet.insert(uf1.root(i));

		int nStates = statesSet.size();

		UF states;
		states.init(0,n);

		sort(edges.begin(),edges.end());

		double ans1=0;
		double ans2=0;

		for ( int i=0;i<edges.size();++i )
		{
			if ( states.root(edges[i].x) != states.root(edges[i].y) )
			{
				if ( uf1.root(edges[i].x) == uf1.root(edges[i].y) )
					ans1 += sqrt( edges[i].w );
				else
					ans2 += sqrt( edges[i].w );
				states.merge(edges[i].x,edges[i].y);
			}
		}
		
		int result1 = ( ans1 - floor(ans1) < floor(ans1)+1-ans1 )?floor(ans1):floor(ans1)+1 ;
		int result2 = ( ans2 - floor(ans2) < floor(ans2)+1-ans2 )?floor(ans2):floor(ans2)+1 ;

		cout << "Case #"<<test<<": "<< nStates << " " << result1 << " " << result2  << endl;

	}
}
