//#include <set>
#include <vector>
//#include <map>
#include <algorithm>
#include <iostream>
#include <cstdio>
using namespace std;

#define MAXN 30
#define INF 1000*1000*1000*1

struct Edge
{
	bool young , bi ;
	int start , end ;
	int w ;
	Edge ( char y , char b , char st , char en , int wei )
	{
		young = (y=='Y');
		bi = (b=='B');
		start = st-'A';
		end = en-'A';
		w = wei;
	}
};

vector<int> fw (bool young,const vector<Edge> edges,int first)
{
	int FW[MAXN][MAXN];
	for ( int i=0;i<MAXN;++i )
	{
		for ( int j=0;j<MAXN;++j )
		{
			FW[i][j]= ( i==j )?0:INF;
		}
	}
	for ( int i=0;i<edges.size();++i )
	{
		Edge e = edges[i];
		if ( e.young != young ) continue;
		FW[e.start][e.end]=std::min(e.w,FW[e.start][e.end]);
		if ( e.bi )
			FW[e.end][e.start]=std::min(e.w,FW[e.end][e.start]);
	}
	for ( int k=0;k<MAXN;++k )
	{
		for ( int i=0;i<MAXN;++i )
		{
			for ( int j=0;j<MAXN;++j )
			{
				FW[i][j]=std::min(FW[i][j],FW[i][k]+FW[k][j]);
			}
		}
	}


	vector< int > res;

	for ( int i=0;i<MAXN;++i )
	{
		res.push_back(FW[first][i]);
	}

	return res;

}

int main()
{
//	freopen("in.in","r",stdin);
//	freopen("out.out","w",stdout);

	while ( true )
	{
		int n;		cin >> n;
		if ( n==0 ) break;
		vector < Edge > edges;
		for ( int i=0;i<n;++i )
		{
			char a,b,c,d;int w;
			cin>>a>>b>>c>>d>>w;
			edges.push_back(Edge(a,b,c,d,w));
		}
		char ch1,ch2 ; cin>>ch1>>ch2;
		vector <int> v1,v2;
		v1 = fw(true,edges,ch1-'A');
		v2 = fw(false,edges,ch2-'A');

		vector < pair<int,int> > res;
		for ( int i=0;i<MAXN;++i )
		{
			res.push_back(make_pair(v1[i]+v2[i],i));
		}
		sort(res.begin(),res.end());
		int mn = res[0].first;
		if ( mn < INF )
		{
			cout << mn ;
			int i=0;
			while ( res[i].first==mn )
			{
				cout << " " << char( 'A'+res[i].second);
				++i;
			}
			cout << endl;
		}
		else
		{
			cout << "You will never meet." << endl;
		}


	}

}
