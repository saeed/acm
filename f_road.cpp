#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

#define MAXN 10000

struct UF
{
	int id[MAXN];
	int sz[MAXN];
	void init(int start,int end)
	{
		for ( int i=start;i<=end;++i )
		{
			id[i]=i;
			sz[i]=1;
		}
	}
	int root (int p)
	{
		while ( id[p]!=p )
		{
			id[p]=id[id[p]];
			p = id[p];
		}
		return p;
	}
	void merge ( const int & p,const int &q )
	{
		int pId = root(p);
		int qId = root(q);
		if ( sz[pId] > sz[qId] )
		{
			sz[pId] += sz[qId];
			id[qId] = id[pId];
		}
		else
		{
			sz[qId] += sz[pId];
			id[pId] = id[qId];
		}
	}
};

struct Edge
{
	int x,y,w;
	Edge(int _x=0,int _y=0,int _w=0):x(_x),y(_y),w(_w) {}
	bool operator < ( const Edge &e) const {return w < e.w;}

};

int main()
{
	freopen("in.in","r",stdin);
	freopen("out.out","w",stdout);
	int testCases;
	cin >> testCases;
	while ( testCases-- )
	{
		int n,m,p,q;
		cin>>n>>m>>p>>q;
		UF uf ;
		uf.init(1,n);
		vector<Edge> edges;
		for ( int i=0;i<m;++i )
		{
			int a,b,c;
			cin>>a>>b>>c;
			edges.push_back(Edge(a,b,c));
		}
		sort(edges.begin(),edges.end());
		//int ans=0;
		bool found = false;
		for ( int i=0;i<m;++i )
		{
			if ( uf.root(edges[i].x) != uf.root(edges[i].y) )
			{
				if ( (p==edges[i].x && q==edges[i].y)||(q==edges[i].x && p==edges[i].y) )
				{
					found = true;
					break;
				}
				//ans+=edges[i].w;
				uf.merge(edges[i].x,edges[i].y);
			}
		}
		if ( found )
			cout << "YES" << endl;
		else
			cout << "NO" << endl;

	}
}