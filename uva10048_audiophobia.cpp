#include <cmath>
#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include <cstring>
using namespace std;

#define MAXN 101

struct UF
{
	int id[MAXN];
	int sz[MAXN];
	void init(int start,int end)
	{
		for ( int i=start;i<=end;++i )
		{
			id[i]=i;
			sz[i]=1;
		}
	}
	int root (int p)
	{
		while ( id[p]!=p )
		{
			id[p]=id[id[p]];
			p = id[p];
		}
		return p;
	}
	void merge ( const int & p,const int &q )
	{
		int pId = root(p);
		int qId = root(q);
		if ( sz[pId] > sz[qId] )
		{
			sz[pId] += sz[qId];
			id[qId] = id[pId];
		}
		else
		{
			sz[qId] += sz[pId];
			id[pId] = id[qId];
		}
	}
};

struct Edge
{
	int x,y;int w;
	Edge(int _x=0,int _y=0,int _w=0):x(_x),y(_y),w(_w) {}
	bool operator < ( const Edge &e) const {return w < e.w;}

};

int dp[101][101];

int dfs (int parent,int source,int dest,const vector<Edge> & mst)
{
	if ( dp[source][dest] != -1)
		return dp[source][dest];
	else if ( dp[dest][source] != -1)
		return dp[dest][source];

	for ( int i=0;i<mst.size();++i )
	{
		Edge e = mst[i];
		if ( e.x==source )
		{
			if ( e.y==dest )
			{
				dp[source][dest]=e.w;
				dp[dest][source]=e.w;
				return e.w;
			}
			else if ( e.y!=parent )
			{
				int r = dfs(source,e.y,dest,mst);
				if ( r != -1 )
				{
					dp[source][dest]=std::max(r,e.w);
					dp[dest][source]=dp[source][dest];
					return dp[source][dest];
				}
			}
		}
		if ( e.y==source )
		{
			if ( e.x==dest )
			{
				dp[source][dest]=e.w;
				dp[dest][source]=e.w;
				return e.w;
			}
			else if ( e.x!=parent )
			{
				int r = dfs(source,e.x,dest,mst);
				if ( r != -1 )
				{
					dp[source][dest]=std::max(r,e.w);
					dp[dest][source]=dp[source][dest];
					return dp[source][dest];
				}
			}
		}
	}
	return -1;
}

int main()
{
	//freopen("in.in","r",stdin);
	//freopen("out.out","w",stdout);
	int test =0;
	while ( true )
	{
		++test;
		memset(dp,-1,sizeof(dp));
		int n,m,q;
		cin >> n >> m>>q;
		if ( n==m && m==0 && q==0)
			break;
		UF uf;
		uf.init(1,n);
		vector<Edge> edges;
		//int total =0;
		for ( int i=0;i<m;++i )
		{
			int a,b,c;cin>>a>>b>>c;
		//	total+=c;
			edges.push_back(Edge(a,b,c));
		}
		sort(edges.begin(),edges.end());

		vector<Edge>mst;

		

		for ( int i=0;i<edges.size();++i )
		{
			if ( uf.root(edges[i].x) != uf.root(edges[i].y) )
			{
				mst.push_back(edges[i]);
				uf.merge(edges[i].x,edges[i].y);
			}
		}

		for ( int i=1;i<n;++i )
		{
			for ( int j=i+1;j<=n;++j )
			{
				dfs(-1,i,j,mst);
			}
		}

		if ( test != 1)
			cout << endl;
		cout <<"Case #"<<test<<endl;;

		for ( int i=0;i<q;++i )
		{
			int a,b;
			cin>>a>>b;
			if ( dp[a][b] == -1 )
				cout << "no path" << endl;
			else if ( a==b )
				cout << 0 << endl;
			else
				cout << dp[a][b] << endl;
		}

		

	}
	
}


