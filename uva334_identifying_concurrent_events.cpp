#include <vector>
#include <string>
#include <iostream>
#include <map>
#include <cstring>

using namespace std;

#define MAXN 1000



int main()
{
	//freopen("in.in","r",stdin);
	int n ;
	int test=0;
	while ( true )
	{
		++test;
		map <string,int> mid;
		map <int,string> rev;
		bool fw [MAXN][MAXN];
		memset(fw,0,sizeof(fw));
		int count = 0;
		cin>>n;
		if ( n==0 ) break;
		for ( int i=0;i<n;++i )
		{
			int t ; cin>>t;
			vector<int> v;
			for ( int j=0;j<t;++j )
			{
				string s ; cin>> s;
				if ( mid.find(s) == mid.end() )
				{
					mid[s]=count;
					rev[count]=s;
					++count;
				}
				v.push_back(mid[s]);
			}
			for ( int j=0;j<t-1;++j )
			{
				fw[v[j]][v[j+1]]=1;
			}
		}
		cin>>n;
		for ( int i=0;i<n;++i )
		{
			string s1,s2;
			cin>>s1>>s2;
			if ( mid.find(s1) == mid.end() )
			{
				mid[s1]=count;
				rev[count]=s1;
				++count;
			}
			
			if ( mid.find(s2) == mid.end() )
			{
				mid[s2]=count;
				rev[count]=s2;
				++count;
			}
			fw [mid[s1]][mid[s2]]=1;
		}

		for ( int k=0;k<count;++k )
		{
			for ( int i=0;i<count;++i )
			{
				for ( int j=0;j<count;++j )
				{
					fw[i][j]=( fw[i][j] | (fw[i][k]&fw[k][j]) );
				}
			}
		}

		int ccount = 0;
		vector <string> res;
		for ( int i=0;i<count-1;++i )
		{
			for ( int j=i+1;j<count;++j )
			{
				if ( fw[i][j]==0 && fw[j][i]==0 )
				{
					ccount++;
					string s = "("+rev[i]+","+rev[j]+")";
					res.push_back ( s );
				}
			}
		}

		if ( ccount )
		{
			cout << "Case "<<test<<", "<<ccount<<" "<<"concurrent events:"<<endl;
			if ( ccount > 1 )
				cout <<res[0]<<" "<<res[1]<<" "<<endl;
			else
				cout <<res[0]<<" "<<endl;
		}
		else
			cout << "Case "<<test<<", no concurrent events."<<endl;



	}
}