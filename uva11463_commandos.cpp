//#include <set>
#include <vector>
//#include <map>
#include <iostream>
#include <cstdio>
using namespace std;

#define MAXN 101
#define INF 1000*1000*1000

int main()
{
//	freopen("in.in","r",stdin);

	int tests; cin>>tests;
	for ( int test=1;test<=tests;++test)
	{
		int n,r;cin>>n>>r;
		vector< pair<int,int> > edges;
		for ( int i=0;i<r;++i )
		{
			int a,b;cin>>a>>b;
			edges.push_back(make_pair(a,b));
			edges.push_back(make_pair(b,a));
		}

		int start,end;
		cin>>start>>end;

		int fw [MAXN][MAXN];

		for ( int I=0;I<n;++I )
				for ( int J=0;J<n;++J )
					fw[I][J]= (I==J?0:INF);

		for ( int i = 0 ; i < edges.size() ; ++i )
			{
				pair<int,int> e = edges[i];
				fw[e.first][e.second] = 1;
			}

		for ( int k=0;k<n;++k )
				for ( int I=0;I<n;++I )
					for ( int J=0;J<n;++J )
						fw[I][J]=std::min(fw[I][J],fw[I][k]+fw[k][J]);

		int mx = -1;
		for ( int i=0;i<n;++i )
		{
			mx = std::max( fw[start][i]+fw[i][end],mx );
		}
		cout << "Case "<<test<<": "<<mx << endl;



	}
	

}