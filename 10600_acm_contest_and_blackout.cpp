#include <cmath>
#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include <cstring>
#include <cstdio>
using namespace std;

#define MAXN 501

struct UF
{
	int id[MAXN];
	int sz[MAXN];
	void init(int start,int end)
	{
		for ( int i=start;i<=end;++i )
		{
			id[i]=i;
			sz[i]=1;
		}
	}
	int root (int p)
	{
		while ( id[p]!=p )
		{
			id[p]=id[id[p]];
			p = id[p];
		}
		return p;
	}
	void merge ( const int & p,const int &q )
	{
		int pId = root(p);
		int qId = root(q);
		if ( sz[pId] > sz[qId] )
		{
			sz[pId] += sz[qId];
			id[qId] = id[pId];
		}
		else
		{
			sz[qId] += sz[pId];
			id[pId] = id[qId];
		}
	}
};

struct Edge
{
	int x,y;int w;
	Edge(int _x=0,int _y=0,int _w=0):x(_x),y(_y),w(_w) {}
	bool operator < ( const Edge &e) const {return w < e.w;}

};


int main()
{
	//freopen("in.in","r",stdin);
	//freopen("out.out","w",stdout);
	int tests ; cin>>tests;
	//memset(sat,0,sizeof(sat));
	for ( int i=0;i<tests;++i )
	{
		int n,m;
		cin >> n >> m;
		UF uf;
		uf.init(1,n);
		vector<Edge> edges;
		vector<pair<int,int> > nodes;
		for ( int i=0;i<m;++i )
		{
			int a,b,c;cin>>a>>b>>c;
			edges.push_back(Edge(a,b,c));
		}
		
		sort(edges.begin(),edges.end());

		vector<int>mst;

		int ans = 0;
		for ( int i=0;i<edges.size();++i )
		{
			if ( uf.root(edges[i].x) != uf.root(edges[i].y) )
			{
				mst.push_back(i);
				ans+=edges[i].w;
				uf.merge(edges[i].x,edges[i].y);
			}
		}
			
		int ans2=10000000000;
		for ( int i=0;i<mst.size();++i )
		{
			uf.init(1,n);
			int res = 0;
			int weight = edges[mst[i]].w ;
			int l = 0;
			edges[mst[i]].w=-1;

			for ( int i=0;i<edges.size();++i )
			{
				if (edges[i].w==-1)
					continue;
				if ( uf.root(edges[i].x) != uf.root(edges[i].y) )
				{
					++l;
					//mst.push_back(i);
					res+=edges[i].w;
					uf.merge(edges[i].x,edges[i].y);
				}
			}
			if ( l==n-1 )
				ans2 = std::min(res,ans2);
			edges[mst[i]].w = weight;

		}

		cout << ans << " " << ans2 << endl;
	}
	
}


